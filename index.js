
const walk = require('fswalk');
const path = require('path');
const child_process = require('child_process');
const fs = require('fs');
const { ArgumentParser } = require('argparse');

const { version, description } = require('./package.json');

function isGreaterThanTargetResHeight (sourcePath, targetResHeight) {
    const cmd = `${path.join('ffmpeg', 'bin', 'ffprobe')} -v error -select_streams v:0 -show_entries stream=width,height -of json "${sourcePath}"`;
    const stuff = child_process.execSync(cmd);
    const json = JSON.parse(stuff.toString());
    const height = json.streams[0].height;
    return height > targetResHeight ? true : false; 
}

function scaleAndCopy (sourcePath, targetPath, targetRes) {
    const cmd = `${path.join('ffmpeg', 'bin', 'ffmpeg')} -loglevel quiet -n -i "${sourcePath}" -vf scale=-1:${targetRes} -c:v libx264 -preset medium  -tune film -c:a copy "${targetPath}"`;
    child_process.execSync(cmd);
}

function createOutputDir(target) {
    // Let's make sure we have a path to the target file or folder.
    const targetExists = fs.existsSync(target);
    
    // If target exists, what is it?
    if (targetExists) {
        if (fs.lstatSync(target).isFile()) {
            throw new Error(`The target ${target} already exists.`);
        }
    } else {
        let parsedPath = path.parse(target);
        // if there is no parent dir then we are passing a relative file path without the dot(s)-slash.
        // lets add that.
        if (parsedPath.dir === '') parsedPath = path.parse(`./${target}`);

        let targetDir;
        if (parsedPath.ext === '') {
            // this is a directory.
            targetDir = target;
        } else {
            // this is a file, get the parent.
            targetDir = parsedPath.dir;
        }
        if (!fs.existsSync(targetDir)) fs.mkdirSync(targetDir, { recursive: true });
    }
}

function scaleAndCopyFile(source, target, targetRes) {
    const targetFile = path.parse(target).ext === '' ? `${target}/${path.parse(source).base}` : target;
    try {
        if (isGreaterThanTargetResHeight(source, targetRes)) {
            const targetDir = path.parse(targetFile).dir;
            if (!fs.existsSync(targetDir)) fs.mkdirSync(targetDir, { recursive: true });
            scaleAndCopy(source, targetFile, targetRes);
            console.log(targetFile);
        }
    } catch (error) {
        console.log(`Error with ${source}: ${error.message}`);
    }
}

function walkScaleAndCopyFiles(source, target, targetRes) {
    walk(source, function (filePath, stats) {
        const parsedPath = path.parse(filePath);
        
        if (fs.lstatSync(filePath).isFile() && (parsedPath.ext === '.mp4' || parsedPath.ext === '.MP4')) {
            const sourcePath = source.split(/\/|\\/).filter(s => s).join(path.sep);
            const parsedPathSpl = parsedPath.dir.split(path.sep).join(path.sep);
            const targetPath = path.join(target, parsedPathSpl.replace(sourcePath, ''));
            const targetFilePath = path.join(targetPath, parsedPath.base);

            scaleAndCopyFile(filePath, targetFilePath, targetRes);
        }
    });
}


function go(source, target, targetRes) {
    if (!fs.existsSync(source)) throw new Error(`Source ${source} does not exist.`);

    createOutputDir(target);

    const sourceStats = fs.lstatSync(source);
    if (sourceStats.isFile()) {
        // scale and copy file if needed.
        scaleAndCopyFile(source, target, targetRes);
    }

    if (sourceStats.isDirectory()) {
        // scale and copy all files if needed.
        walkScaleAndCopyFiles(source, target, targetRes)
    }
}

const parser = new ArgumentParser({
    description: description,
});

parser.add_argument('-v', '--version', { action: 'version', version });
parser.add_argument('-s', '--source', { help: 'The source file or folder.' });
parser.add_argument('-d', '--destination', {
    help: 'The destination file if source is a file or destination folder.',
    nargs: '?', default: '.',
});
parser.add_argument('-r', '--res', { help: 'The desired height to which the source file(s) will be scaled.' }); 
const args = parser.parse_args();



go (args.source, args.destination, args.res);
